;function strapdownjs(window, document) {

//////////////////////////////////////////////////////////////////////
//
// Shims for IE < 9
//

document.head = document.getElementsByTagName('head')[0];

if (!('getElementsByClassName' in document)) {
	document.getElementsByClassName = function(name) {
		function getElementsByClassName(node, classname) {
			var a = [];
			var re = new RegExp('(^| )'+classname+'( |$)');
			var els = node.getElementsByTagName("*");
			for(var i=0,j=els.length; i<j; i++)
				if(re.test(els[i].className))a.push(els[i]);
					return a;
		}
		return getElementsByClassName(document.body, name);
	}
}

//////////////////////////////////////////////////////////////////////
//
// Get user elements we need
//

var titleEl = document.getElementsByTagName('title')[0],
scriptEls = document.getElementsByTagName('script'),
navbarEl = document.getElementsByClassName('navbar')[0];

//////////////////////////////////////////////////////////////////////
//
// <head> stuff
//

// Use <meta> viewport so that Bootstrap is actually responsive on mobile
var metaEl = document.createElement('meta');
metaEl.name = 'viewport';
metaEl.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0';
if (document.head.firstChild)
	document.head.insertBefore(metaEl, document.head.firstChild);
else
	document.head.appendChild(metaEl);

// Get origin of script
var origin = '';
for (var i = 0; i < scriptEls.length; i++) {
	if (scriptEls[i].src.match('strapdown')) {
		origin = scriptEls[i].src;
	}
}
var originBase = origin.substr(0, origin.lastIndexOf('/'));

// Get theme
var theme = document.body.getAttribute('theme') || 'bootstrap';
theme = theme.toLowerCase();

// Stylesheets
var linkEl = document.createElement('link');
linkEl.href = originBase + '/themes/'+theme+'.min.css';
linkEl.rel = 'stylesheet';
document.head.appendChild(linkEl);

var linkEl = document.createElement('link');
linkEl.href = originBase + '/strapdown.css';
linkEl.rel = 'stylesheet';
document.head.appendChild(linkEl);

var linkEl = document.createElement('link');
linkEl.href = originBase + '/themes/bootstrap-responsive.min.css';
linkEl.rel = 'stylesheet';
document.head.appendChild(linkEl);

//////////////////////////////////////////////////////////////////////
//
// <body> stuff

//
// Insert navbar if there's none
var newNode = document.createElement('div');
newNode.className = 'navbar navbar-fixed-top';
if (!navbarEl && titleEl) {
	newNode.innerHTML = '<div class="navbar-inner"> <div class="container"> <div id="headline" class="brand"> </div> </div> </div>';
	document.body.insertBefore(newNode, document.body.firstChild);
	var title = titleEl.innerHTML;
	var headlineEl = document.getElementById('headline');
	if (headlineEl)
		headlineEl.innerHTML = title;
}


var markdownElements = [],i;
Array.prototype.push.apply(markdownElements, document.getElementsByTagName('xmp'));
//Array.prototype.push.apply(markdownElements, document.getElementsByTagName('textarea'));
//var markdownElements = document.getElementsByTagName('xmp'), i;
for(i = 0; i < markdownElements.length; i++){
	var markdownEl = markdownElements[i];
	var markdown = markdownEl.textContent || markdownEl.innerText;

	var newNode = document.createElement('article');
	newNode.className = 'container';
	newNode.id = 'md-content-'+i;
	markdownEl.parentNode.replaceChild(newNode, markdownEl);


	//////////////////////////////////////////////////////////////////////
	//
	// Markdown!
	//

	// Generate Markdown
	var html = marked(markdown);
	newNode.innerHTML = html;

	// Prettify
	var codeEls = newNode.getElementsByTagName('code');
	for (var i=0, ii=codeEls.length; i<ii; i++) {
		var codeEl = codeEls[i];
		var lang = codeEl.className;
		codeEl.className = 'prettyprint lang-' + lang;
	}
	prettyPrint();

	// Style tables
	var tableEls = newNode.getElementsByTagName('table');
	for (var i=0, ii=tableEls.length; i<ii; i++) {
		var tableEl = tableEls[i];
		tableEl.className = 'table table-striped table-bordered';
	}
}

}
