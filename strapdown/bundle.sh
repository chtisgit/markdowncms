#!/bin/sh

ver=$1
if [ -z "$ver" ]; then
	echo 'You need to specify the version'
	exit 1
fi

outDir='v/'$ver

if [ -d "$outDir" ]; then
	rm -Rf $outDir
fi
mkdir -p $outDir

printf '\n' > newline
cat vendor/marked.min.js newline vendor/prettify.min.js newline src/strapdown.js > "$outDir/strapdown.js"

cp src/strapdown.css $outDir
mkdir -p "$outDir/themes"
cp vendor/themes/* "$outDir/themes/"

