<?php

const NOCHANGE = "<no change>";

function endsWith($haystack, $needle)
{
	$length = strlen($needle);
	if ($length == 0) {
		return true;
	}

	return (substr($haystack, -$length) === $needle);
}

$pages_json_contents_var = false;
function pages_json_contents($init = false)
{
	global $pages_json_contents_var;
	if($init != false){
		$pages_json_contents_var = $init;
	}else if($pages_json_contents_var === false){
		$pages_json_contents_var = file_get_contents(PAGES_JSON);
	}
	return $pages_json_contents_var;
}

function save()
{
	if(isset($_POST["themechooser"])){
		$p = json_decode(pages_json_contents(),true);
		$p["theme"] = $_POST["themechooser"];
		if(file_put_contents(PAGES_JSON,json_encode($p)) === false){
			$success = false;
			$msg = "could not change theme";
		}
	}else if(isset($_POST["filename"]) && isset($_POST["contents"])){
		if(strchr($_POST["filename"],'/') != false) return;
		if(file_put_contents("data/".$_POST["filename"].".md", $_POST["contents"]) === false){
			$success = false;
			$msg = "could not save article";
		}
		if(isset($_POST["targetpage"]) && $_POST["targetpage"] != NOCHANGE){
			# TODO: change page
		}
	}
}

function files($subdir,$ext='')
{
	$dir = opendir("$subdir");

	for(;;){
		$n = readdir($dir);
		if($n === false) break;
		$name = htmlspecialchars($n);
		if($name[0] != '.' && endsWith($name,$ext)){
			$a[] = $name;
		}
	}
	closedir($dir);

	return $a;
}

const PASSWORD_FILE = "data/password";
const PAGES_JSON = "data/pages.json";
const PAGES_JSON_INIT = "{\"pages\":{\"index\":[]}}";
const THEMES_DIR = "strapdown/v/blog/themes";

$action = "";
$msg = "";
$success = true;
$loggedin = false;

session_start();

if(file_exists(PASSWORD_FILE)){
	$pwhash = file_get_contents(PASSWORD_FILE);
}else{
	$action = "setpw";
}
if(isset($_POST['password'])){
	$givenpw = $_POST['password'];
}

// parse and process request

if(isset($givenpw) && isset($_POST['newpassword1']) && isset($_POST['newpassword2'])){
	$action="setpw";
	if($_POST['newpassword1'] === $_POST['newpassword2']){
		if(!isset($pwhash) || password_verify($givenpw, $pwhash)){
			file_put_contents(PASSWORD_FILE, password_hash($_POST['newpassword1'],PASSWORD_BCRYPT));
			$msg = "password updated successfully!";
		}else{
			$msg = "old password was incorrect!";
			$success = false;
		}
	}else{
		$msg = "passwords don't match!";
		$success = false;
	}
}
if($action != "setpw" && isset($givenpw) && isset($pwhash)){
	if(password_verify($givenpw, $pwhash)){
		$_SESSION["sess_loggedin"] = true;
		$msg = "Login successfull";
	}else{
		$action = "login";
		$msg = "Login failed";
		$success = false;
	}
}
$loggedin = isset($_SESSION["sess_loggedin"]) && $_SESSION["sess_loggedin"] === true;

if(isset($loggedin) && $loggedin === true){
	save();
}

if($action === ""){
	if(isset($_GET["action"])){
		$action = $_GET["action"];
	}else if($loggedin === true){
		$action = "edit";
	}else{
		$action = "login";
	}
}
$pages_list = NULL;
$themes_list = NULL;
$current_theme = '';
if($loggedin != true){
	usleep(200000);
}else{
	if(!file_exists(PAGES_JSON)){
		if(file_put_contents(PAGES_JSON, pages_json_contents(PAGES_JSON_INIT)) === false){
			$success = false;
			$msg = "incorrect permissions for data/ directory";
		}
	}
	if($success === true){
		$pages_list = json_decode(pages_json_contents(),true);
		if($pages_list === NULL || !isset($pages_list["pages"])){
			$success = false;
			$msg = "pages.json file is damaged";
		}
		if(isset($pages_list["theme"]))
			$current_theme = $pages_list["theme"];
		$pages_list = $pages_list["pages"];
	}
	$themes_list = files(THEMES_DIR,'.css');

	if($action === "logout"){
		$_SESSION["sess_loggedin"] = false;
	}
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Admin page</title>
</head>

<body style="font-family:monospace;padding-bottom:50px">
<script src="strapdown/v/blog/strapdown.js"></script>
<script src="admin.js"></script>


<?php
if(strlen($msg) > 0){
	$msg_style = "font-size:12pt;background-color:#222222;padding:5px;";
	if($success==true){
		echo '<div style="'.$msg_style.';color:#00FF00">'."$msg</div>";
	}else{
		echo '<div style="'.$msg_style.'color:#FF0000">'."$msg</div>";
	}
}
?>

<?php

// SET NEW PASSWORD
if($action === "setpw"){

?>

<div style="width:300px;margin:auto;margin-top:50px;border:1px solid black;padding:20px">
<form action="admin.php" method="post">
old password:<br>
<input type="password" name="password"><br>
new password:<br>
<input type="password" name="newpassword1"><br> 
retype new password:<br>
<input type="password" name="newpassword2"><br>
<input type="submit" value="Change password">
</form>
</div>

<?php

// ------------------- SETPW END
}else if($action === "edit" && !isset($editfile)){

?>
<a style="float:right;margin: 0px 10px 10px 0px;font-size:14pt" href="?action=logout">Logout</a>
<h1>Edit</h1>

<div style="padding:0px 50px">
<h2>Pages</h2><ul>

<?php
if($pages_list != NULL){
	foreach($pages_list as $name => $arr){
		echo "<li><span style=\"font-size:12pt\">$name</span></li>";
		if(count($arr) > 0){
			echo "<ul>";
			foreach($arr as $name){
				echo "<li>Article <a href=\"#\" onclick=\"edit('$name')\">$name</a></li>";
			}

			echo "</ul>";
		}
	}
}
?>
</ul>
<br><hr><br>

<script>
var pages_json = <?php echo pages_json_contents(); ?> ;
</script>

<h2>Add Page</h2>
<form method="post">
<table border="0" width="350px">
<tr><td>Name:</td><td><input type="text" name="pagename" width="100%"></td></tr>
<tr><td></td><td><input type="submit" value="New Page"></td></tr>
</table>
</form>

<br><hr><br>

<h2>Choose Theme</h2>

Current Theme: <b><?php echo $current_theme; ?></b><br><br>

<form method="post">
<select name="themechooser">
<?php
if($themes_list != NULL){
	foreach($themes_list as $name){
		echo "<option value=\"$name\">$name</option>";
	}
}
?>
</select>
<input type="submit" value="Activate">

</form>


<br><hr><br>

<h2>Write article</h2>

<form method="post">
<table border="0" width="92%">
<tr><td>File:</td><td><input type="text" id="write_filename" name="filename" width="100%"></td></tr>
<tr><td>Page:</td><td>
<select name="targetpage">
<option value="<?php echo NOCHANGE; ?>">&lt;no change&gt;</option>
<?php
if($pages_list != NULL){
	foreach($pages_list as $name => $arr){
		echo "<option value=\"$name\">$name</option>";
	}
}
?>
</select>
</td></tr>
<tr><td></td><td><textarea id="write_textarea" name="contents" style="min-width:300px;width:100%;min-height:200px"></textarea></td></tr>
<tr><td></td><td>
	<input type="submit" value="Save">
	<button type="button" onclick="preview('<?php echo $current_theme; ?>')">Preview</button>
</td></tr>
</table>
</form>
<div id="preview_container" style="width:100%;padding:15px"></div>

<br><hr><br>

<h2>Settings</h2>

<button onclick="grandupdate()">Grand update button</button>
<button>Grand reset button</button>

<br>

<div id="updatelog" style="padding:10px;background-color:#333333;color:white;font-size:12pt">
<table border="0">
<tr>
<td><b>Progress</b></td>
<td><progress id="gup" value="0" max="100" style="min-width:300px;min-height:20px">
</progress></td>
</tr><tr>
<td><b>Update status:</b></td>
<td><span id="updatestatus">
</span></td>
</tr>
</table>
</div>

<br><br>
<a href="admin.php?action=setpw">Reset password</a>

</div>

<?php
// ------------------- EDIT END
}else{

?>

<div style="width:300px;margin:auto;margin-top:50px;border:1px solid black;padding:20px">
<form action="admin.php" method="post">
password:<br>
<input type="password" name="password"><br>
<input type="submit" value="Login">
</form>
</div>

<?php
// ------------------- LOGIN END
}
?>

</body>
</html>
