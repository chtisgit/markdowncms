function httpGet(theUrl, callback, err_callback, async = true)
{
	var xmlHttp = new XMLHttpRequest();
	if(async){
		xmlHttp.onreadystatechange = function() { 
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
				callback(xmlHttp.responseText);
			else
				err_callback();
		}
	}
	xmlHttp.open("GET", theUrl, async);
	xmlHttp.send(null);
	if(!async){
		if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
			callback(xmlHttp.responseText);
			return true;
		}
		err_callback();
		return false;
	}
}
function httpPost(params, callback, err_callback)
{
	var xmlHttp = new XMLHttpRequest();
	/* xmlHttp.onreadystatechange = function() { 
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
			callback(xmlHttp.responseText);
		else
			err_callback();
	} */
	xmlHttp.open("POST", "admin.php", false);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.send(params);
	return xmlHttp.readyState == 4 && xmlHttp.status == 200;
}

function edit(file)
{
	var x = document.getElementById('write_filename');
	x.value = file;
	httpGet("data/"+file+".md", function(response){
		document.getElementById("write_textarea").value = response;
	}, function(){
		document.getElementById("write_textarea").value = '';
	});
}
function editSync(file)
{
	var x = document.getElementById('write_filename');
	x.value = file;
	return httpGet("data/"+file+".md", function(response){
		document.getElementById("write_textarea").value = response;
	}, function(){
		document.getElementById("write_textarea").value = '';
	}, false);
}

function theme_name(t)
{
	var i = t.indexOf('.');
	if(i == -1)
		return t;
	return t.slice(0,i);
}

function preview(theme,display=true)
{
	var C = document.getElementById('preview_container');
	var iframe = document.createElement('iframe');
	var xmp = document.createElement('xmp');
	var text = document.getElementById("write_textarea").value;
	var p = document.createElement('span');

	if(display) C.style.display="block"; else C.style.display="none";

	if(text.search("</xmp") != -1){
		alert("Cannot generate preview");
		return;
	}

	var html = '<xmp>'+text+'</xmp>';
	html += '<script src="strapdown/v/blog/strapdown.js"></script>';
	while(C.firstChild){
		C.removeChild(C.firstChild);
	}
	iframe.style.width = '100%';
	iframe.style.minHeight = '500px';
	C.style.minHeight = '500px';

	p.style.fontSize = '16pt';
	p.textContent = 'Preview';
	C.style.textAlign = 'center';
	C.appendChild(p);
	C.appendChild(iframe);
	iframe.contentWindow.document.open();
	iframe.contentWindow.document.write(html);
	iframe.contentWindow.document.close();
	iframe.contentWindow.document.body.setAttribute('theme',theme_name(theme));
	strapdownjs(iframe.contentWindow, iframe.contentDocument);
	var sc = iframe.contentWindow.document.getElementsByTagName('script');
	var i;
	for(i = 0; i < sc.length; i++){
		sc[i].parentNode.removeChild(sc[i]);
	}
}

function update(page,articles)
{
	var pbar = document.getElementById('gup');
	var ust = document.getElementById('updatestatus');
	if(!httpPost("action=resetpage&page="+encodeURIComponent(page),null,null))
		return false;
	for(var i = 0; i < articles.length; i++){
		pbar.value++;
		ust.textContent = "Updating page "+page+" - Article \""+articles[i]+"\"";
		if(!editSync(articles[i])){
			return false;
		}
		preview(pages_json.theme,false);
		var iframe = document.getElementById('preview_container').getElementsByTagName('iframe')[0];
		var chtml = iframe.contentWindow.document.getElementsByTagName('article')[0].parentNode.innerHTML;
		if(!httpPost("action=appendpage&page="+encodeURIComponent(page)+"&content="+encodeURIComponent(chtml),null,null)){
			return false;
		}
	}
	if(!httpPost("action=finalizepage&page="+encodeURIComponent(page),null,null))
		return false;
	pbar.value++;

	return true;
}

function grandupdate()
{
	var count = 0;
	var pbar = document.getElementById('gup');
	var ust = document.getElementById('updatestatus');
	var keys = Object.keys(pages_json.pages);
	
	for(var i = 0; i < keys.length; i++){
		count += 1 + pages_json.pages[keys[i]].length;
	}
	pbar.max = count;
	pbar.value = 0;
	for(var i = 0; i < keys.length; i++){
		if(!update(keys[i], pages_json.pages[keys[i]])){
			ust.textContent = "Update failed!";
			return;
		}
	}
	ust.textContent = "Update successful!";
}
